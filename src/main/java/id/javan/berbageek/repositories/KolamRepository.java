package id.javan.berbageek.repositories;

import id.javan.berbageek.model.Kolam;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by winzaldi on 27/06/19.
 */
public interface KolamRepository extends CrudRepository<Kolam,Long>{
}
