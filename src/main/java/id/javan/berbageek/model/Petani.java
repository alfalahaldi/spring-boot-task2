package id.javan.berbageek.model;

import javax.persistence.*;

/**
 * Created by winzaldi on 27/06/19.
 */
@Entity
public class Petani {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private  Long id;

    @Column(name = "nama_lengkap")
    private  String namaLengkap;

    @Column(name = "no_telp")
    private  String noTelp;

//    @OneToMany(mappedBy = "petani")
//    private List<Kolam> kolams;

    public  Petani(){}

//    public  Petani(String namaLengkap,String noTelp,List<Kolam> kolams){
//        this.setNamaLengkap(namaLengkap);
//        this.setNoTelp(noTelp);
//        this.setKolams(kolams);
//    }

    public  Petani(String namaLengkap,String noTelp){
        this.setNamaLengkap(namaLengkap);
        this.setNoTelp(noTelp);
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamaLengkap() {
        return namaLengkap;
    }

    public void setNamaLengkap(String namaLengkap) {
        this.namaLengkap = namaLengkap;
    }

    public String getNoTelp() {
        return noTelp;
    }

    public void setNoTelp(String noTelp) {
        this.noTelp = noTelp;
    }

//    public List<Kolam> getKolams() {
//        return kolams;
//    }
//
//    public void setKolams(List<Kolam> kolams) {
//        this.kolams = kolams;
//    }
}
