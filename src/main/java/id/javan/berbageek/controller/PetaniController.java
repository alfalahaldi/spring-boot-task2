package id.javan.berbageek.controller;

import id.javan.berbageek.exception.ResourceNotFoundException;
import id.javan.berbageek.model.Petani;
import id.javan.berbageek.repositories.PetaniRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by winzaldi on 01/07/19.
 */

@RestController
@RequestMapping("/api/v1")
public class PetaniController {

    @Autowired
    private PetaniRepository petaniRepository;

    @GetMapping("/petani")
    public Iterable<Petani> getAllPetani() {
        return petaniRepository.findAll();
    }


    @GetMapping("/petani/{id}")
    public ResponseEntity<Petani> getPetaniById(@PathVariable(value = "id") Long petaniId)
            throws ResourceNotFoundException {
        Petani petani = petaniRepository.findById(petaniId)
                .orElseThrow(() -> new ResourceNotFoundException("Petani not found for this id :: " + petaniId));
        return ResponseEntity.ok().body(petani);
    }

    @PostMapping("/petani")
    public Petani createPetani(@Valid @RequestBody Petani petani) {
        return petaniRepository.save(petani);
    }

    @PutMapping("/petani/{id}")
    public ResponseEntity<Petani> updatePetani(@PathVariable(value = "id") Long petaniId,
                                                   @Valid @RequestBody Petani petaniDetails) throws ResourceNotFoundException {

        Petani petani = petaniRepository.findById(petaniId)
                .orElseThrow(() -> new ResourceNotFoundException("Petani not found for this id :: " + petaniId));

        petani.setNamaLengkap(petaniDetails.getNamaLengkap());
        petani.setNoTelp(petaniDetails.getNoTelp());

        final Petani updatedPetani = petaniRepository.save(petani);
        return ResponseEntity.ok(updatedPetani);
    }

    @DeleteMapping("/petani/{id}")
    public Map<String, Boolean> deletePetani(@PathVariable(value = "id") Long petaniId)
            throws ResourceNotFoundException {
        Petani petani = petaniRepository.findById(petaniId)
                .orElseThrow(() -> new ResourceNotFoundException("Petani not found for this id :: " + petaniId));

        petaniRepository.delete(petani);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }




}
