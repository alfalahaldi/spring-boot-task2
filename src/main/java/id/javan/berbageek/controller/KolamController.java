package id.javan.berbageek.controller;

import id.javan.berbageek.exception.ResourceNotFoundException;
import id.javan.berbageek.model.Kolam;
import id.javan.berbageek.model.Petani;
import id.javan.berbageek.repositories.KolamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by winzaldi on 01/07/19.
 */
@RestController
@RequestMapping("/api/v1")
public class KolamController {

    @Autowired
    private KolamRepository kolamRepository;


    @GetMapping("/allkolam")
    public Iterable<Kolam> getAllKolam() {
        return kolamRepository.findAll();
    }


    @GetMapping("/kolam/{id}")
    public ResponseEntity<Kolam> getKolamById(@PathVariable(value = "id") Long kolamId)
            throws ResourceNotFoundException {
        Kolam kolam = kolamRepository.findById(kolamId)
                .orElseThrow(() -> new ResourceNotFoundException("Kolam not found for this id :: " + kolamId));
        return ResponseEntity.ok().body(kolam);
    }

    @PostMapping("/kolam")
    public Kolam createKolam(@Valid @RequestBody Kolam kolam, Petani petani) {
        kolam.setPetani(petani);
        return kolamRepository.save(kolam);
    }

    @PutMapping("/kolam/{id}")
    public ResponseEntity<Kolam> updateKolam(@PathVariable(value = "id") Long kolamId,
                                               @Valid @RequestBody Kolam kolamDetails,Petani petani) throws ResourceNotFoundException {

        Kolam kolam = kolamRepository.findById(kolamId)
                .orElseThrow(() -> new ResourceNotFoundException("Kolam not found for this id :: " + kolamId));

        kolam.setPetani(petani);
        kolam.setLuas(kolamDetails.getLuas());
        final Kolam updatedKolam = kolamRepository.save(kolam);
        return ResponseEntity.ok(updatedKolam);
    }

    @DeleteMapping("/kolam/{id}")
    public Map<String, Boolean> deleteKolam(@PathVariable(value = "id") Long kolamId)
            throws ResourceNotFoundException {
        Kolam kolam = kolamRepository.findById(kolamId)
                .orElseThrow(() -> new ResourceNotFoundException("Kolam not found for this id :: " + kolamId));

        kolamRepository.delete(kolam);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }



}
