import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PetaniListComponent } from './petani-list.component';

describe('PetaniListComponent', () => {
  let component: PetaniListComponent;
  let fixture: ComponentFixture<PetaniListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PetaniListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PetaniListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
