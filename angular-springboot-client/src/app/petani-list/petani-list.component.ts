import { Observable } from "rxjs";
import { PetaniService } from "./../petani.service";
import { Petani } from "./../petani";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: 'app-petani-list',
  templateUrl: './petani-list.component.html',
  styleUrls: ['./petani-list.component.css']
})
export class PetaniListComponent implements OnInit {

  petanis: Observable<Petani[]>;
  constructor(private petaniService: PetaniService) {}

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.petanis = this.petaniService.getPetanisList();
  }

  deletePetani(id: number) {
    this.petaniService.deletePetani(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

}
