import { TestBed, inject } from '@angular/core/testing';

import { PetaniService } from './petani.service';

describe('PetaniService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PetaniService]
    });
  });

  it('should be created', inject([PetaniService], (service: PetaniService) => {
    expect(service).toBeTruthy();
  }));
});
