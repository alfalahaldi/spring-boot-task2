import { Component, OnInit } from '@angular/core';
import { PetaniService } from './../petani.service';
import { Petani } from './../petani';

@Component({
  selector: 'app-create-petani',
  templateUrl: './create-petani.component.html',
  styleUrls: ['./create-petani.component.css']
})
export class CreatePetaniComponent implements OnInit {

  petani: Petani = new Petani();
  submitted = false;

  constructor(private petaniService: PetaniService) { }

  ngOnInit() {
  }

  newPetani(): void {
    this.submitted = false;
    this.petani = new Petani();
  }

  save() {
    this.petaniService.createPetani(this.petani)
      .subscribe(data => console.log(data), error => console.log(error));
    this.petani = new Petani();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

}
