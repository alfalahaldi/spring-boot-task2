import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePetaniComponent } from './create-petani.component';

describe('CreatePetaniComponent', () => {
  let component: CreatePetaniComponent;
  let fixture: ComponentFixture<CreatePetaniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatePetaniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatePetaniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
