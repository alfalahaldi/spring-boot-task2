import { PetaniDetailsComponent } from './petani-details/petani-details.component';
import { CreatePetaniComponent } from './create-petani/create-petani.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PetaniListComponent } from './petani-list/petani-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'petani', pathMatch: 'full' },
  { path: 'petani', component: PetaniListComponent },
  { path: 'add', component: CreatePetaniComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }