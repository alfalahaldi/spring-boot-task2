import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PetaniDetailsComponent } from './petani-details.component';

describe('PetaniDetailsComponent', () => {
  let component: PetaniDetailsComponent;
  let fixture: ComponentFixture<PetaniDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PetaniDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PetaniDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
