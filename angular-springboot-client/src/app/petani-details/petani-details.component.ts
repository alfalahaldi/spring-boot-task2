import { Petani } from './../petani';
import { Component, OnInit, Input } from '@angular/core';
import { PetaniService } from '../petani.service';
import { PetaniListComponent } from '../petani-list/petani-list.component';


@Component({
  selector: 'app-petani-details',
  templateUrl: './petani-details.component.html',
  styleUrls: ['./petani-details.component.css']
})
export class PetaniDetailsComponent implements OnInit {
  @Input() petani: Petani;
  
  constructor(private petaniService: PetaniService, private listComponent: PetaniListComponent) { }

  ngOnInit() {
  }

}
