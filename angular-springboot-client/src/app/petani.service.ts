import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class PetaniService {


  private baseUrl = '/api/v1/petani';

  constructor(private http: HttpClient) { }

  getPetani(id: number): Observable<Object> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createPetani(petani: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, petani);
  }

  updatePetani(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deletePetani(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  getPetanisList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }

}
